msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-28 00:19+0000\n"
"PO-Revision-Date: 2023-02-24 12:43\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/plasma-nm/kcm_mobile_hotspot.pot\n"
"X-Crowdin-File-ID: 12220\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "KDE 中国, csslayer, Tyson Tan"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kde-china@kde.org, csslayer@kde.org, tysontan@tysontan.com"

#: hotspotsettings.cpp:18
#, kde-format
msgid "Hotspot"
msgstr "无线热点"

#: hotspotsettings.cpp:19
#, kde-format
msgid "Tobias Fella"
msgstr "Tobias Fella"

#: package/contents/ui/main.qml:23
#, kde-format
msgid "Enabled:"
msgstr "启用："

#: package/contents/ui/main.qml:35
#, kde-format
msgid "SSID:"
msgstr "SSID："

#: package/contents/ui/main.qml:41
#, kde-format
msgid "Password:"
msgstr "密码："

#: package/contents/ui/main.qml:46
#, kde-format
msgid "Save"
msgstr "保存"
