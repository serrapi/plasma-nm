# Fuminobu TAKEYAMA <ftake@geeko.jp>, 2015, 2019.
# Tomohiro Hyakutake <tomhioo@outlook.jp>, 2019, 2020, 2021.
# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.plasma.networkmanagement\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-18 01:01+0000\n"
"PO-Revision-Date: 2021-09-25 16:16-0700\n"
"Last-Translator: Fumiaki Okushi <fumiaki.okushi@gmail.com>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Generator: Lokalize 21.08.1\n"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Connect"
msgstr "接続"

#: contents/ui/ConnectionItem.qml:60
#, kde-format
msgid "Disconnect"
msgstr "切断"

#: contents/ui/ConnectionItem.qml:85
#, fuzzy, kde-format
#| msgid "Show network's QR code"
msgid "Show Network's QR Code"
msgstr "ネットワークの QR コードを表示"

#: contents/ui/ConnectionItem.qml:90
#, kde-format
msgid "Configure…"
msgstr "設定…"

#: contents/ui/ConnectionItem.qml:121
#, kde-format
msgid "Speed"
msgstr "速度"

#: contents/ui/ConnectionItem.qml:126
#, kde-format
msgid "Details"
msgstr "情報"

#: contents/ui/ConnectionItem.qml:169
#, kde-format
msgctxt "@info:tooltip"
msgid ""
"Current download speed is %1 kibibytes per second; current upload speed is "
"%2 kibibytes per second"
msgstr ""

#: contents/ui/ConnectionItem.qml:286
#, kde-format
msgid "Connected, ⬇ %1/s, ⬆ %2/s"
msgstr "接続済み, ⬇ %1/秒, ⬆ %2/秒"

#: contents/ui/ConnectionItem.qml:290
#, kde-format
msgid "Connected"
msgstr "接続済み"

#: contents/ui/DetailsText.qml:45
#, kde-format
msgid "Copy"
msgstr "コピー"

#: contents/ui/main.qml:24
#, kde-format
msgid "Networks"
msgstr "ネットワーク"

#: contents/ui/main.qml:33
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn off Airplane Mode"
msgstr ""

#: contents/ui/main.qml:35
#, kde-format
msgctxt "@info:tooltip"
msgid "Middle-click to turn on Airplane Mode"
msgstr ""

#: contents/ui/main.qml:79 contents/ui/Toolbar.qml:69
#, kde-format
msgid "Enable Wi-Fi"
msgstr "Wi-Fi を有効化"

#: contents/ui/main.qml:87
#, fuzzy, kde-format
#| msgid "Enable mobile network"
msgid "Enable Mobile Network"
msgstr "モバイルネットワークを有効化"

#: contents/ui/main.qml:95
#, kde-format
msgid "Enable Airplane Mode"
msgstr ""

#: contents/ui/main.qml:101
#, kde-format
msgid "Open Network Login Page…"
msgstr "ネットワークのログインページを開く…"

#: contents/ui/main.qml:106
#, fuzzy, kde-format
#| msgid "&Configure Network Connections..."
msgid "&Configure Network Connections…"
msgstr "ネットワーク接続を設定(&C)..."

#: contents/ui/main.qml:143
#, kde-format
msgctxt "@title:window"
msgid "QR Code for %1"
msgstr ""

#: contents/ui/PasswordField.qml:16
#, kde-format
msgid "Password…"
msgstr "パスワード…"

#: contents/ui/PopupDialog.qml:102
#, kde-format
msgid "Airplane mode is enabled"
msgstr "機内モードが有効です"

#: contents/ui/PopupDialog.qml:106
#, kde-format
msgid "Wireless and mobile networks are deactivated"
msgstr ""

#: contents/ui/PopupDialog.qml:108
#, kde-format
msgid "Wireless is deactivated"
msgstr "ワイヤレスネットワークは無効です"

#: contents/ui/PopupDialog.qml:111
#, kde-format
msgid "Mobile network is deactivated"
msgstr ""

#: contents/ui/PopupDialog.qml:114
#, kde-format
msgid "No matches"
msgstr ""

#: contents/ui/PopupDialog.qml:116
#, kde-format
msgid "No available connections"
msgstr "利用可能な接続なし"

#: contents/ui/Toolbar.qml:118
#, kde-format
msgid "Enable mobile network"
msgstr "モバイルネットワークを有効化"

#: contents/ui/Toolbar.qml:143
#, kde-kuit-format
msgctxt "@info"
msgid "Disable airplane mode<nl/><nl/>This will enable Wi-Fi and Bluetooth"
msgstr "機内モードを無効化<nl/><nl/>Wi-Fi と Bluetooth を有効化します"

#: contents/ui/Toolbar.qml:144
#, kde-kuit-format
msgctxt "@info"
msgid "Enable airplane mode<nl/><nl/>This will disable Wi-Fi and Bluetooth"
msgstr "機内モードを有効化<nl/><nl/>Wi-Fi と Bluetooth を無効化します"

#: contents/ui/Toolbar.qml:154
#, kde-format
msgid "Hotspot"
msgstr "ホットスポット"

#: contents/ui/Toolbar.qml:178 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Disable Hotspot"
msgstr "ホットスポットを無効化"

#: contents/ui/Toolbar.qml:183 contents/ui/Toolbar.qml:189
#, kde-format
msgid "Create Hotspot"
msgstr "ホットスポットを作成"

#: contents/ui/Toolbar.qml:221
#, kde-format
msgid "Configure network connections…"
msgstr "ネットワーク接続を設定…"

#: contents/ui/TrafficMonitor.qml:36 contents/ui/TrafficMonitor.qml:106
#, kde-format
msgid "/s"
msgstr "/s"

#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Upload"
msgstr "アップロード"

#: contents/ui/TrafficMonitor.qml:88
#, kde-format
msgid "Download"
msgstr "ダウンロード"

#, fuzzy
#~| msgid "Connect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Connect to %1"
#~ msgstr "接続"

#, fuzzy
#~| msgid "Disconnect"
#~ msgctxt "@info:tooltip %1 is the name of a network connection"
#~ msgid "Disconnect from %1"
#~ msgstr "切断"
